# phusion passenger docker is a minimal distro based on ubuntu with nice
# docker-related features
FROM phusion/passenger-customizable:0.9.20

ENV HOME /root

RUN apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get update \
    && /pd_build/python.sh \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
